# Readstadon

![Readstodon Logo - an elephant reading](resources/readaphant.jpg)

A tool to update Mastodon from your GoodReads activity feed.

- CLI: `updates` - command to list updates
- CLI: `mastodon_setup` - register the app and save user credentials
- CLI: `post` - post updates from GoodReads to Mastodon

These features depend on several environment vars, best stored in a `.env` file in the same directory this code is running.

## CLI options

```
usage: Readstadon [-h] {mastodon_setup,updates,post} ...

Mastodon cross poster for Goodreads activity

positional arguments:
  {mastodon_setup,updates,post}

optional arguments:
  -h, --help            show this help message and exit
```

### List Updates

```
usage: Readstadon updates [-h] [--recent]

List GoodReads activity

optional arguments:
  -h, --help  show this help message and exit
  --recent    List only activities that have posted since the last run.
```

### Post Updates

```
usage: Readstadon post [-h] [--recent]

Post GoodReads activity to Mastodon

optional arguments:
  -h, --help  show this help message and exit
  --recent    Re-post only activities that have posted since the last run.```
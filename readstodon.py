"""
Readstodon

Mastodon cross poster for Goodreads activity
"""

import os
import pprint
# import sys
import argparse
import hashlib
import requests
from requests_html import HTML
import tempfile

import goodreads_api_client as goodreads_api_client
from mastodon import Mastodon

LATEST_FILE = ".latest_hash"


def _get_hash(update):
    """
    Get a hash of an update
    """
    return hashlib.sha1(repr(update).encode('utf-8')).hexdigest()


def _save_latest_hash(update):
    """
    Store the hash of the latest update so we can compare later
    """
    with open(LATEST_FILE, "w") as last_file:
        last_file.write(_get_hash(update))


def _get_latest_hash():
    if os.path.exists(LATEST_FILE):
        with open(LATEST_FILE, "r") as last_file:
            return last_file.read()
    return ""


def _get_review_id(update):
    return update['object']['read_status']['review_id']['#text']


def _get_updates(user_id, developer_key, update_type, recent=False):
    gr = goodreads_api_client.Client(developer_key=developer_key)
    user = gr.User.show(user_id)

    # print(user['updates']['update'])

    updates = [
        u for u in user['updates']['update']
        if u['@type'] == 'readstatus'
        ]

    if recent:
        last_hash = _get_latest_hash()
        if last_hash:
            for i, update in enumerate(updates):
                if _get_hash(update) == last_hash:
                    updates = updates[:i]
    if updates:
        # might be an empty list if there's nothing new
        _save_latest_hash(updates[0])

    # for update in updates:
    #     review_id = _get_review_id(update)
    #     review = _get_review(review_id, developer_key)
    #     update['image_url'] = review['book']['image_url']

    return updates


def _get_review(review_id, developer_key):
    gr = goodreads_api_client.Client(developer_key=developer_key)
    review = gr.Review.show(review_id)
    return review


def _upload_review_image(url, client):
    """
    Download the review image then upload as
    media to Mastodon, returns the media id
    """
    print(f"Creating media for {url}")
    filename = os.path.basename(url)
    d = tempfile.gettempdir()
    fpath = os.path.join(d, filename)
    print(f"saving media to {fpath}")
    resp = requests.get(url)
    with open(fpath, 'w+b') as f:
        f.write(resp.content)

    media_data = client.media_post(fpath)
    media_id = media_data['id']

    print(f"Created media {media_id}")
    return media_id


def gr_updates_cmd(args):

    KEY = os.environ.get("GOODREADS_KEY")
    USER_ID = os.environ.get("GOODREADS_USER")

    updates = _get_updates(USER_ID, KEY, 'read_status', args.recent)
    for update in updates:
        print(
            f"• {update['actor']['name']} {update['action_text']} ")


def mastodon_setup_cmd(args):
    """
    Setup application secret and user credentials
    """
    print(f"Setting up Readstadon app: {args.app}")

    if not os.path.exists(args.secret):
        Mastodon.create_app(
            args.app,
            api_base_url=args.instance,
            to_file=args.secret
        )

    if not os.path.exists(args.credentials):
        mastodon = Mastodon(
            client_id=args.secret,
            api_base_url=args.instance
        )
        mastodon.log_in(
            args.user,
            args.password,
            to_file=args.credentials,
        )


def post_updates_cmd(args):
    instance = os.environ.get('MASTODON_INSTANCE')
    mastodon = Mastodon(
        access_token='.credentials',
        api_base_url=instance,
    )
    developer_key = os.environ.get("GOODREADS_KEY")
    user_id = os.environ.get("GOODREADS_USER")

    updates = _get_updates(user_id, developer_key, 'read_status', args.recent)

    if not updates:
        print("No new GoodReads activity found")
        return

    updates.reverse()

    for update in updates:
        # media_id = _upload_review_image(update['image_url'], mastodon)
        links = HTML(html=update['action_text']).absolute_links
        link = list(links)[0]
        name = update['actor']['name']
        title = update['object']['read_status']['review']['book']['title']
        status = update['object']['read_status']['status']

        if status == "to-read":
            # ignore
            continue

        verb = {
            "currently-reading": "is reading",
            "read": "finished",
        }.get(status)

        toot = f"""
        {name} {verb} {title}

        {link}
        """
        print(f"Posting update: {toot}")
        toot = mastodon.status_post(
            toot,
            # media_ids=[media_id]
        )
        print(f"Posted update {toot['url']}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        "Readstadon",
        description="Mastodon cross poster for Goodreads activity")

    subparsers = parser.add_subparsers()

    mastodon_setup = subparsers.add_parser("mastodon_setup")
    mastodon_setup.set_defaults(command=mastodon_setup_cmd)
    mastodon_setup.add_argument("app")
    mastodon_setup.add_argument(
        "--instance",
        default=os.environ.get("MASTODON_INSTANCE"))
    mastodon_setup.add_argument(
        "-u", "--user",
        default=os.environ.get("MASTODON_USER"))
    mastodon_setup.add_argument(
        "-p", "--password",
        default=os.environ.get("MASTODON_PWD"))
    mastodon_setup.add_argument(
        "--secret", default=".secret")
    mastodon_setup.add_argument(
        "--credentials", default=".credentials")

    updates = subparsers.add_parser(
        "updates", description="List GoodReads activity")
    updates.set_defaults(command=gr_updates_cmd)
    updates.add_argument(
        "--recent", action="store_true",
        help="List only activities that have posted since the last run."
    )

    post = subparsers.add_parser(
        "post", description="Post GoodReads activity to Mastodon")
    post.set_defaults(command=post_updates_cmd)
    post.add_argument(
        "--recent", action="store_true",
        help="Re-post only activities that have posted since the last run."
    )

    args = parser.parse_args()
    args.command(args)
